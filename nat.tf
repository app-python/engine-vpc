# nat gw
resource "aws_eip" "main-nat" {
  vpc = true
  tags = {
    Name       = "eip_nat_main_${var.env}"
    Terraform  = true
    Ambiente   = var.env
    APP        = "main"
    Projeto    = "main"
    Requerente = var.requerente
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.main-nat.id
  subnet_id     = aws_subnet.vpc-public-0.id
  depends_on = [
    aws_internet_gateway.vpc-gw
  ]
  tags = {
    Name                                                   = "nat_gtw_main_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    modalidade                                             = "projeto"
  }

  lifecycle {
    create_before_destroy = true
  }
}