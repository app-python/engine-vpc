
resource "aws_subnet" "vpc-private-0" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_APP_A
  map_public_ip_on_launch = false
  availability_zone       = var.main_APP_A

  tags = {
    Name                                                   = "main_APP_A_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/internal-elb"                      = "1"
    modalidade                                             = "projeto"
  }
}

resource "aws_subnet" "vpc-private-1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_APP_B
  map_public_ip_on_launch = false
  availability_zone       = var.main_APP_B

  tags = {
    Name                                                   = "main_APP_B_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/internal-elb"                      = "1"
    modalidade                                             = "projeto"
  }
}


resource "aws_subnet" "vpc-private-2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_APP_C
  map_public_ip_on_launch = "false"
  availability_zone       = var.main_APP_C

  tags = {
    Name                                                   = "main_APP_C_${var.env}"
    Terraform                                              = "true"
    Ambiente                                               = "${var.env}"
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = "${var.requerente}"
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/internal-elb"                      = "1"
    modalidade                                             = "projeto"
  }
}


resource "aws_subnet" "vpc-private-3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_APP_D
  map_public_ip_on_launch = "false"
  availability_zone       = var.main_APP_D

  tags = {
    Name                                                   = "main_APP_D_${var.env}"
    Terraform                                              = "true"
    Ambiente                                               = "${var.env}"
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = "${var.requerente}"
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/internal-elb"                      = "1"
    modalidade                                             = "projeto"
  }
}