resource "aws_subnet" "vpc-public-0" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_DMZ_A
  map_public_ip_on_launch = true
  availability_zone       = var.main_DMZ_A

  tags = {
    Name                                                   = "main_DMZ_A_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/elb"                               = "1"

    modalidade = "projeto"

  }
}

resource "aws_subnet" "vpc-public-1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_DMZ_B
  map_public_ip_on_launch = true
  availability_zone       = var.main_DMZ_B

  tags = {
    Name                                                   = "main_DMZ_B_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/elb"                               = "1"
    modalidade                                             = "projeto"

  }
}

resource "aws_subnet" "vpc-public-2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_DMZ_C
  map_public_ip_on_launch = true
  availability_zone       = var.main_DMZ_C

  tags = {
    Name                                                   = "main_DMZ_C_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/elb"                               = "1"
    modalidade                                             = "projeto"

  }
}

resource "aws_subnet" "vpc-public-3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.CIDR_main_DMZ_D
  map_public_ip_on_launch = true
  availability_zone       = var.main_DMZ_D

  tags = {
    Name                                                   = "main_DMZ_D_${var.env}"
    Terraform                                              = true
    Ambiente                                               = var.env
    APP                                                    = "main"
    Projeto                                                = "main"
    Requerente                                             = var.requerente
    "kubernetes.io/cluster/${var.cluster-name}-${var.env}" = "shared"
    "kubernetes.io/role/elb"                               = "1"
    modalidade                                             = "projeto"

  }
}
